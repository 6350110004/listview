import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    'B1',
    'B2',
    'B3',
    'B4',
    'B5',
    'B6',
    'B7',
    'B8',
    'B9'
  ];

  final pic = [
    AssetImage('assets/1.jpg'),
    AssetImage('assets/2.jpg'),
    AssetImage('assets/3.jpg'),
    AssetImage('assets/4.jpg'),
    AssetImage('assets/5.jpg'),
    AssetImage('assets/6.jpg'),
    AssetImage('assets/7.jpg'),
    AssetImage('assets/8.jpg'),
    AssetImage('assets/9.jpg')
  ];

  final subtitle =[
    "Hello 1",
    "Hello 2",
    "Hello 3",
    "Hello 4",
    "Hello 5",
    "Hello 6",
    "Hello 7",
    "Hello 8",
    "Hello 9"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: pic[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: Icon(
                  Icons.add_circle,
                  size: 25,
                ),
                onTap: (){
                  Fluttertoast.showToast(msg:'${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
              ),
              Divider(
                thickness: 1,)
            ],
          );
        },
      ),
    );
  }
}
